# README #

1) Clone the application using git clone https://sparsh1994@bitbucket.org/sparsh1994/user-authentication.git

2) npm install

3) node index

4) Application is also deployed on heroku : https://edwisor-user.herokuapp.com/

5) Message api is a sample api which will be called after login, so it user authentication.

6) Have used both ES5 and ES6 syntax and used babel to transpile the ES6 to ES5.

7) Password should be encrypted while storing in DB and should be decrypted while fetching for matching in authentication, but as per instructions we can't use 3rd party library so haven't used and encryption algo.
