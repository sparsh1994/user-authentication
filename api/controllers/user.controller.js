const User = require('../models/user.model.js');
const jwt = require('jsonwebtoken');
const config = require('../config/app.config');

export const createUser = (req, res) => {
  const user = new User(req.body);
  user.save(function(err) {
    if (err) throw err;
    res.json({ success: true });
  });
}

export const getUsers = (req, res) => {
    User.find({}, (err, users) => {
        res.json(users);
      });
}

export const loginUser = (req, res) => {
    User.findOne({
        name: req.body.name
      }, (err, user) => {
    
        if (err) throw err;
    
        if (!user) {
          res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {
    
          // password check
          if (user.password != req.body.password) {
            res.json({ success: false, message: 'Authentication failed. Wrong password.' });
          } else {
    
        const payload = { admin: user.admin };
            const token = jwt.sign(payload, config.secret, { expiresIn: 60 * 60 });
    
            // return token as JSON
            res.json({
              success: true,
              message: 'Token creation successful',
              token: token
            });
          } 
        }
    
      });
}

export const getMessage = (req, res) => {
    res.send('Sample Message');
}
