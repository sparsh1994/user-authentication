import { createUser, getUsers, loginUser, getMessage } from '../controllers/user.controller';
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

module.exports = function (app, apiRoutes) {

    apiRoutes.use((req, res, next) => {
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        // decode token
        if (token) {
            console.log(req.query, token)
            // verifies secret and checks exp
            jwt.verify(token, app.get('superSecret'), function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            // if no token attached
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    });

    app.use('/api', apiRoutes);

    // route to be accessed after login
    apiRoutes.get('/message', (req, res) => {
        getMessage(req, res);
    });

    /* Create */
    app.post('/user', (req, res) => {
        createUser(req, res);
    });

    /* get users */
    app.get('/users', (req, res) => {
        getUsers(req, res);
    });

    /* user login */
    app.post('/login', (req, res) => {
        loginUser(req, res);
    });

}