export const Validators = {
    NameValidator: /[A-Z][a-z]{1,20}$/g,
    FullNameValidation: /[A-Z][a-zA-Z][^#&<>\"~;$^%{}?]{1,20}$/g,
    PanNumberValidaton: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/,
    DOB: /^\d{2}[\/.]\d{2}[\/.]\d{2}$/g
}