const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');

const config = require('./api/config/app.config'); // get our config file

// configuration
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret constiable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));
const apiRoutes = express.Router();
const userRoutes = require('./api/routes/user.route.js')(app, apiRoutes);

const server = app.listen(config.port, function () {
    console.log('Server running on port' + config.port);
});